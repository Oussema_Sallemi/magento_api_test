export const state = () => ({
  customer_token: ""
});

export const getters = {
  getToken: state => {
    return state.customer_token;
  }
};

export const mutations = {
  SET_TOKEN: (state, payload) => {
    state.customer_token = payload;
  }
};
