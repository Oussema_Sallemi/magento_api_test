export const state = () => {
  return {
    cart: { quote_id: 0, items: [] }
  };
};
export const mutations = {
  ADD_TO_CART(state, payload) {
    state.cart.items = [...state.cart.items, ...payload[1]];
    state.cart.quote_id = payload[0];
    console.log(state.cart);
  },
  EMPTY_CART(state) {
    state.cart.items.splice(0, state.cart.items.length);
    state.cart.quote_id = 0;
    console.log(state.cart);
  }
};

export const actions = {
  addToCart({ commit }, payload) {
    commit("ADD_TO_CART", payload);
  },
  emptyCart({ commit }) {
    commit("EMPTY_CART");
  }
};

export const getters = {
  getCart: state => {
    return state.cart;
  }
};
