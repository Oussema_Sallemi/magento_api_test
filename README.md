## Api calls need to be specific cause we don't have an api call for top level menu categories like Chaussures/Vetements/...

## Testing with just one category for level 1 and level 2 pages ==> response for the whole category takes too much time

## Product prices from the Api response are not the same as the prices from the backend example product GANTS X-RUN ULTRA Soft Shell LIGHT with the SKU GWLTRA9908

## Still didn't try the product page + add of configurable and bundle products + auth logic

## The category imported has 18 products only 4 of them appear in site, i chose GARMIN SWIM 2 for testing

## Recommendation : create a seperate database for the NUXTJS frontend to invoke cause every API takes along time to be processed

## We ned to bring the url key of a product out of the custom attribute so that we can access it consistantly for every product

## Deleting quote or deleteing items from quote demand an API

## using router.push gives us the ability to pass ids in the router params object but breaks app when using browser back button => need to find an other way to summon categories maybe with url_keys
